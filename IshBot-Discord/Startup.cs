﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        private readonly LogSeverity defaultSeverity = LogSeverity.Info;
        
        public Startup(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("_configuration.json");
            Configuration = builder.Build();
            if (int.TryParse(Configuration["logseverity"], out int sev))
            {
                // Set the default severity in the config: Critical = 0, Error = 1, Warning = 2, Info = 3, Verbose = 4, Debug = 5
                // When publishing, set to 3.
                defaultSeverity = (LogSeverity)sev;
            }
        }

        public static async Task RunAsync(string[] args)
        {
            Console.WindowWidth = 180;
            Startup startup = new Startup(args);
            await startup.RunAsync();
        }

        public async Task RunAsync()
        {
            using (ServiceProvider serviceProvider = ConfigureServices(out IServiceCollection collection))
            {
                serviceProvider.GetRequiredService<CommandHandler>();
                serviceProvider.GetRequiredService<Random>();
                LoggingService loggingService = serviceProvider.GetRequiredService<LoggingService>();
                loggingService.LogSeverity = defaultSeverity;
                loggingService.Register(collection);
                
                await serviceProvider.GetRequiredService<StartupService>().StartAsync();
                await Task.Delay(-1);
            }
        }

        private ServiceProvider ConfigureServices(out IServiceCollection collection)
        {
            IServiceCollection myCollection = new ServiceCollection()
                .AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
                {
                    LogLevel = defaultSeverity,
                    MessageCacheSize = 1000
                }))
            .AddSingleton(new CommandService(new CommandServiceConfig
            {
                LogLevel = defaultSeverity,
                DefaultRunMode = RunMode.Async,
                CaseSensitiveCommands = false
            }))
            // All Services MUST inherit either IshBot_Discord.Services.Service 
            //                               or IshBot_Discord.Services.LoggableService
            .AddSingleton<StartupService>()
            .AddSingleton<LoggingService>()
            .AddSingleton<CommandHandler>()

            .AddSingleton<DebugChannelService>()
            .AddSingleton<SovVoiceTagService>()
            .AddSingleton<ReactionRoleService>()
            .AddSingleton<MinesweeperService>()
            // Add Services above this line.
            .AddSingleton<Random>()
            .AddSingleton(Configuration);
            collection = myCollection;
            return myCollection.BuildServiceProvider();
        }
    }
}
