﻿using System;

namespace IshBot_Discord.Extensions
{
    public static class Extensions
    {
        public static ConsoleMessage WithItem(this ConsoleMessage consoleMessage, string text, ConsoleColor consoleColor = ConsoleColor.White)
        {
            consoleMessage.AddItem(text, consoleColor);
            return consoleMessage;
        }
    }
}
