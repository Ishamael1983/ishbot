﻿using Discord;
using Discord.Commands;
using System.Threading.Tasks;

namespace IshBot_Discord.Modules
{
    public class InfoModule : ModuleBase<SocketCommandContext>
    {
        [Command("info")]
        [Summary("Displays bot info.")]
        public async Task BotInfoAsync()
        {
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.WithTitle("About Me");
            embedBuilder.WithDescription("WIP");
            await Context.Channel.SendMessageAsync("", false, embedBuilder.Build());
        }
    }
}
