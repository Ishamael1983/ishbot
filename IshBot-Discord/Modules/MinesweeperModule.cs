﻿using Discord.Commands;
using System.Threading.Tasks;

namespace IshBot_Discord.Modules
{
    public class MinesweeperModule : ModuleBase<SocketCommandContext>
    {
        private readonly MinesweeperService _minesweeper;
        public MinesweeperModule(MinesweeperService minesweeperService)
        {
            _minesweeper = minesweeperService;
        }

        [Command("minesweeper")]
        public async Task Generate(int size = 1)
        {
            string result = await _minesweeper.Generate(Context.Channel.Id, size);
            if (result != "ERRINVALIDCHAN")
                await ReplyAsync(Context.User.Mention + "\n" + result);
        }

        [Command("randomminesweeper")]
        public async Task GenerateRandom()
        {
            string result = await _minesweeper.Generate(Context.Channel.Id);
            if (result != "ERRINVALIDCHAN")
                await ReplyAsync(Context.User.Mention + "\n" + result);
        }
        /*public async Task Generate(int width = -1, int height = -1, int bombCount = -1)
        {
            string result = Context.User.Mention + "\n" + _minesweeper.Generate(width, height, bombCount);
            await ReplyAsync(result);
        }//*/

        [Command("minesweeperhelp")]
        public async Task Help()
        {
            string result = _minesweeper.Help(Context.Channel.Id);
            if (result != "ERRINVALIDCHAN")
                await ReplyAsync(Context.User.Mention + "\n" + result);
        }
    }
}
