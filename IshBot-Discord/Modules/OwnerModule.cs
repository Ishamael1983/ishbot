﻿using Discord.Commands;
using System.Threading.Tasks;

namespace IshBot_Discord.Modules
{
    public class OwnerModule : ModuleBase<SocketCommandContext>
    {
        private readonly DebugChannelService _debug;
        public OwnerModule(DebugChannelService debug)
        {
            _debug = debug;
        }
        [Command("updategame")]
        [RequireOwner]
        public async Task UpdateGameInfo([Remainder]string newGame)
        {
            await Context.Client.SetGameAsync(newGame);
        }

        [Command("DebugEnabled")]
        [RequireOwner]
        public async Task DebugEnabled(bool enabled = false)
        {
            _debug.Enabled = enabled;
            await ReplyAsync($"DebugEnabled: {_debug.Enabled}");
        }

        [Command("SetDebugChannel")]
        [RequireOwner]
        public async Task SetDebugChannel()
        {
            if (_debug.SetChannel(Context.Channel.Id).Result)
                await ReplyAsync("Debug channel set.");
        }
    }
}
