﻿using Discord.Commands;
using System.Threading.Tasks;

namespace IshBot_Discord.Modules
{
    public class SovVoiceTagModule : ModuleBase<SocketCommandContext>
    {
        private readonly SovVoiceTagService _voiceTagService;

        public SovVoiceTagModule(SovVoiceTagService voiceTagService)
        {
            _voiceTagService = voiceTagService;
        }

        [Command("DoVoiceScan")]
        public async Task DoVoiceScan()
        {
            string reply = "";
            reply = await _voiceTagService.UserScan();
            await ReplyAsync(reply);
        }
    }
}
