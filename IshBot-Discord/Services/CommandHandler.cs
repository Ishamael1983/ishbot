﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System;
using IshBot_Discord.Services;
using Discord;

namespace IshBot_Discord
{
    public class CommandHandler : LoggableService
    {
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly IConfigurationRoot _config;
        private readonly IServiceProvider _provider;
        private readonly DebugChannelService _debug;

        public CommandHandler(
            DiscordSocketClient discord,
            CommandService commands,
            IConfigurationRoot config,
            IServiceProvider provider,
            DebugChannelService debug)
        {
            _discord = discord;
            _commands = commands;
            _config = config;
            _provider = provider;
            _debug = debug;
            _discord.MessageReceived += OnMessageReceivedAsync;
        }

        private async Task OnMessageReceivedAsync(SocketMessage s)
        {
            if (!(s is SocketUserMessage msg)) return;
            if (msg.Author.Id == _discord.CurrentUser.Id) return;
            var context = new SocketCommandContext(_discord, msg);
            int argPos = 0;
            if (msg.HasStringPrefix(_config["prefix"], ref argPos) )// || msg.HasMentionPrefix(_discord.CurrentUser, ref argPos)) 
                //                                    remove   ->  )//  <-  from the above line to also respond to mentions
            {
                var result = await _commands.ExecuteAsync(context, argPos, _provider);
                if (result.IsSuccess)
                    await ThrowLog(UsageReport(msg), LogSeverity.Info);
            }
        }

        private string UsageReport(SocketUserMessage msg)
        {
            SocketGuildChannel channel = (SocketGuildChannel)msg.Channel;
            return $"Command used by: `{msg.Author.Username}#{msg.Author.Discriminator}` | Command: `{msg.Content}` | Channel: `#{channel.Name}` | Guild: `{channel.Guild.Name}`";
        }
    }
}
