﻿using Discord;
//using Discord.Rpc;
using Discord.WebSocket;
using DiscordRPC;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class RichPresenceService : IDisposable
    {
        public event Func<LogMessage, Task> Log;
        private void ThrowLog(string text, LogSeverity severity)
        {
            LogMessage lm = new LogMessage(severity, "SovVoiceTag", text);
            Log?.Invoke(lm);
        }

        private readonly IConfigurationRoot _config;
        private readonly DiscordSocketClient _primaryDiscord;
        private readonly Random _random;
        private DiscordRpcClient _rpcDiscord;
        private List<RichPresence> _richPresences;

        public int PresenceListCount => _richPresences.Count;

        public RichPresenceService(IConfigurationRoot config, DiscordSocketClient discord, Random random)
        {
            _config = config;

            _primaryDiscord = discord;
            _random = random;
            _primaryDiscord.Ready += PrimaryDiscord_Ready;
        }

        private Task PrimaryDiscord_Ready()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            string discordToken = _config["tokens:discord"];
            if (string.IsNullOrWhiteSpace(discordToken))
                throw new Exception("Discord token not found. Check config file.");
            _rpcDiscord = new DiscordRpcClient(discordToken);
            _rpcDiscord.OnReady += RpcDiscord_OnReady;
            _rpcDiscord.Initialize();
        }

        private void RpcDiscord_OnReady(object sender, DiscordRPC.Message.ReadyMessage args)
        {
            //todo
            //load _richPresences from json
            //create example and save if no _richPresences
            //otherwise
            //if int specified, load specific (maxed to count-1)
            //else load random
        }

        public async Task SetPresence(int index)
        {
            StringBuilder result = new StringBuilder();
            if (index < 0)
            {
                int rand = _random.Next(0, PresenceListCount);
                SetPresence(_richPresences[rand]);
                result.Append("null");
            }
            else if (index < PresenceListCount)
            {
                SetPresence(_richPresences[index]);
                result.Append("null");
            }
            else
            {
                result.Append("RichPresence index not found. ");
                if (PresenceListCount > 0)
                    result.Append($"There are {PresenceListCount} presences saved.");
            }

            await Task.FromResult(result);
        }
        private void SetPresence(RichPresence richPresence)
        {
            _rpcDiscord.SetPresence(richPresence);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~RichPresenceService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        private static RichPresence ExampleRichPresence()
        {
            RichPresence richPresence = new RichPresence()
            {
                Assets = new Assets()
                {
                    LargeImageKey = "LargeImageKey",
                    LargeImageText = "LargeImageText",
                    SmallImageKey = "SmallImageKey"
                },
                Details = "Example Details",
                State = "Example State"
            };
            return richPresence;
        }
    }
}
