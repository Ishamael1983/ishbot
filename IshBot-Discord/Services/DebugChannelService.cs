﻿using Discord;
using Discord.WebSocket;
using IshBot_Discord.Services;
using System;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class DebugChannelService : LoggableService
    {
        private readonly LoggingService _loggingService;
        private readonly DiscordSocketClient _discord;

        private ulong _guildID;
        private ulong _channelID = 484011308807880717;
        private SocketTextChannel targetChannel;
        private bool previouslyEnabled = false;

        public bool Enabled { get; set; } = false;

        public DebugChannelService(LoggingService loggingService, DiscordSocketClient discord)
        {
            _loggingService = loggingService;
            _loggingService.Log += LoggingService_Log;
            _discord = discord;
            _discord.Ready += Discord_Ready;
            _discord.Disconnected += Discord_Disconnected;            
        }

        private async Task LoggingService_Log(LogMessage msg)
        {
            if (msg.Exception == null)
            {
                if (Enabled || (msg.Source == "CommandHandler"))
                    await SendText($"{msg.Source}: {msg.Message}");
            }
        }

        private Task Discord_Disconnected(Exception arg)
        {
            previouslyEnabled = Enabled;
            Enabled = false;
            return Task.CompletedTask;
        }

        private async Task Discord_Ready()
        {
            if (SetChannel(_channelID).Result)
            {
                Enabled = Enabled ? true : previouslyEnabled;
                await SendText("IshBot: Online");
            }
        }

        private async Task SendText(string text)
        {
            if (targetChannel != null)
                await targetChannel.SendMessageAsync(text);
        }

        public async Task<bool> SetChannel(ulong channelID)
        {
            _channelID = channelID;
            SocketGuildChannel channel = (SocketGuildChannel)_discord.GetChannel(_channelID);
            _guildID = channel.Guild.Id;
            targetChannel = _discord.GetGuild(_guildID).GetTextChannel(_channelID);
            bool result = targetChannel.Id == channelID;
            if (!result)
                await ThrowLog("Unable to set output channel.", LogSeverity.Error);
            return result;
        }
    }
}
