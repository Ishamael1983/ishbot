﻿using Discord;
using Discord.WebSocket;
using IshBot_Discord.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class MinesweeperService : LoggableService
    {

        private readonly MinesweeperWrapper _minesweeper = new MinesweeperWrapper();
        private readonly MinesweeperCooldown _mscd = new MinesweeperCooldown();
        private readonly DiscordSocketClient _discord;
        private readonly Random _random;
        private readonly List<ulong> rules = new List<ulong>();

        public MinesweeperService(DiscordSocketClient discord, Random random)
        {
            _discord = discord;
            _random = random;
            SetCooldowns();
            AddRules();
        }
        private void SetCooldowns()
        {
            _mscd.CooldownSmall = 90;
            _mscd.CooldownMedium = 360;
            _mscd.CooldownLarge = 720;
        }
        private void AddRules()
        {
            rules.Add(541311799727882241); // 1
            rules.Add(541311927054237704); // 2
            rules.Add(541086496481083436); // LeePHX
            rules.Add(541089425866883074); // FC
            rules.Add(541268544747470888); // SOV Main
        }

        /// <summary>
        /// Generate a "random" Minesweeper puzzle.
        /// </summary>
        /// <param name="channelID"></param>
        /// <returns></returns>
        public async Task<string> Generate(ulong channelID)
        {
            int randomWidth = _random.Next(MinesweeperSettings.MinWidth, MinesweeperSettings.MaxWidth);
            MinesweeperSettings ms = new MinesweeperSettings(randomWidth, _random);
            if (rules.Contains(channelID))
                if (_mscd.Query(4, channelID, ms.Cooldown))
                {
                    string result = await Generate(ms.Width, ms.Height, ms.BombCount);
                    await ThrowLog($"Random map (width:{ms.Width}|height:{ms.Height}|bombs:{ms.BombCount}) generated.", LogSeverity.Verbose);
                    await ThrowLog($"Raw: {result}", LogSeverity.Debug);
                    return result;
                }
            return "ERRINVALIDCHAN";
        }
        /// <summary>
        /// Generate a Minesweeper puzzle.
        /// </summary>
        /// <param name="channelID"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public async Task<string> Generate(ulong channelID, int size)
        {
            if (rules.Contains(channelID))
                if (_mscd.Query(size, channelID))
                {
                    string result = _minesweeper.Generate(size);
                    if (!string.IsNullOrEmpty(result))
                    {
                        await ThrowLog($"Map (size {size}) generated.", LogSeverity.Info);
                        await ThrowLog($"Raw: {result}", LogSeverity.Debug);
                    }
                    return result;
                }
            return "ERRINVALIDCHAN";
        }
        public async Task<string> Generate(int width, int height, int bombCount)
        {
            bool tooBig = true;
            int attempts = 0;
            int maxAttempts = 10;
            string result;

            while (tooBig && (attempts++ < maxAttempts))
            {
                result = _minesweeper.Generate(width, height, bombCount);
                tooBig = result.Length > 2000;
                if (!tooBig)
                {
                    await ThrowLog($"Map [{width}|{height}|{bombCount}] generated.", LogSeverity.Verbose);
                    await ThrowLog($"Raw: {result}", LogSeverity.Debug);
                    return result;
                }
                else
                {
                    await ThrowLog($"Map [{width}|{height}|{bombCount}] is too large.", LogSeverity.Verbose);
                    await ThrowLog($"Raw: {result}", LogSeverity.Debug);
                }
            }
            return $"Error: Unable to generated puzzle within 2000 characters. `{--attempts}` attempts were made.";
        }
        public string Help(ulong channelID)
        {
            if (rules.Contains(channelID))
                return GetCombinedHelp(channelID);
            return "ERRINVALIDCHAN";
        }
        private string GetCombinedHelp(ulong channelID)
        {
            if (rules.Contains(channelID))
                return _minesweeper.Help() + "\n" + _mscd.Help();
            return "ERRINVALIDCHAN";
        }
    }
}
