﻿namespace IshBot_Discord.Services
{
    public abstract class Service
    {
        public bool IsLoggable => (LoggableService)this != null;
    }
}
