﻿using Discord;
using Discord.WebSocket;
using IshBot_Discord.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class ReactionRoleService : LoggableService
    {

        private readonly DiscordSocketClient _discord;
        private readonly List<ReactionRoleModel> _models = new List<ReactionRoleModel>();

        public ReactionRoleService(DiscordSocketClient discord)
        {
            _discord = discord;
            _discord.ReactionAdded += Discord_ReactionAdded;
            _discord.ReactionRemoved += Discord_ReactionRemoved;

            AddDefaultRules();
        }

        private void AddDefaultRules()
        {
            _models.Add(new ReactionRoleModel
            {
                RoleName = "Agent Inq humanity check.",
                ServerID = 418516310192816129,
                MessageID = 532950705166876673,
                EmojiID = 580874302765662216, // :ai2:
                RoleID = 455762845834149888, // @Community
                NoticeRemovals = false,
                InvertBehaviour = false
            });
            _models.Add(new ReactionRoleModel
            {
                RoleName = "LeePHX Lobby",
                ServerID = 379806642666995723,
                MessageID = 551195912949727233,
                EmojiID = 548233331171655682,
                RoleID = 514628030535041037,
                NoticeRemovals = false,
                InvertBehaviour = false
            });
            _models.Add(new ReactionRoleModel
            {
                RoleName = "Fantasticon Lobby Going",
                ServerID = 430461292017614850,
                MessageID = 563393350204653568,
                EmojiUnicode = "\U0001F389", // Tada
                RoleID = 430462090751246368
            });
            _models.Add(new ReactionRoleModel
            {
                RoleName = "Fantasticon Lobby Want-to-go",
                ServerID = 430461292017614850,
                MessageID = 563393350204653568,
                EmojiUnicode = "\U0001F440", // Eyes
                RoleID = 430462049097875457
            });
            _models.Add(new ReactionRoleModel
            {
                RoleName = "Ecrodorious' humanity check.",
                ServerID = 319515929786580992,
                MessageID = 613768734452219971,
                EmojiUnicode = "\U0001F918", // Metal
                RoleID = 320398118539624449
            });
            _models.Add(new ReactionRoleModel
            {
                RoleName = "Fantasticon change-my-tag",
                ServerID = 430461292017614850,
                MessageID = 614445406033805313,
                EmojiID = 489185341161340930,
                RoleID = 430462090751246368,
                NegativeRoleID = 430462049097875457,
                NoticeRemovals = true
            });
        }

        private async Task Discord_ReactionRemoved(Cacheable<IUserMessage, ulong> arg1, ISocketMessageChannel arg2, SocketReaction arg3)
        {
            if (TryGetRule(arg1.Id, arg3, out ReactionRoleModel roleModel))
            {
                if (roleModel.NoticeRemovals)
                {
                    SocketGuildUser targetUser = _discord.GetGuild(roleModel.ServerID).GetUser(arg3.UserId);
                    IRole targetRole = _discord.GetGuild(roleModel.ServerID).GetRole(roleModel.RoleID);
                    await ThrowLog($"Rule:`{roleModel.RoleName}` | User:`{targetUser.Username}@{targetUser.Discriminator}` | {targetRole.Name} Removed.", LogSeverity.Info);
                    if (roleModel.InvertBehaviour)
                    {
                        await targetUser.AddRoleAsync(targetRole);
                        if (roleModel.NegativeRoleID > 0)
                        {
                            IRole negativeRole = _discord.GetGuild(roleModel.ServerID).GetRole(roleModel.NegativeRoleID);
                            await targetUser.RemoveRoleAsync(negativeRole);
                        }
                    }
                    else
                    {
                        await targetUser.RemoveRoleAsync(targetRole);
                        if (roleModel.NegativeRoleID > 0)
                        {
                            IRole negativeRole = _discord.GetGuild(roleModel.ServerID).GetRole(roleModel.NegativeRoleID);
                            await targetUser.AddRoleAsync(negativeRole);
                        }
                    }
                }
            }
        }

        private async Task Discord_ReactionAdded(Cacheable<IUserMessage, ulong> arg1, ISocketMessageChannel arg2, SocketReaction arg3)
        {
            if (TryGetRule(arg1.Id, arg3, out ReactionRoleModel roleModel))
            {
                SocketGuildUser targetUser = _discord.GetGuild(roleModel.ServerID).GetUser(arg3.UserId);
                IRole targetRole = _discord.GetGuild(roleModel.ServerID).GetRole(roleModel.RoleID);
                await ThrowLog($"Rule:`{roleModel.RoleName}` | User:`{targetUser.Username}@{targetUser.Discriminator}` | {targetRole.Name} Added.", LogSeverity.Info);
                if (roleModel.InvertBehaviour)
                {
                    await targetUser.RemoveRoleAsync(targetRole);
                    if (roleModel.NegativeRoleID > 0)
                    {
                        IRole negativeRole = _discord.GetGuild(roleModel.ServerID).GetRole(roleModel.NegativeRoleID);
                        await targetUser.AddRoleAsync(negativeRole);
                    }
                }
                else
                {
                    await targetUser.AddRoleAsync(targetRole);
                    if (roleModel.NegativeRoleID > 0)
                    {
                        IRole negativeRole = _discord.GetGuild(roleModel.ServerID).GetRole(roleModel.NegativeRoleID);
                        await targetUser.RemoveRoleAsync(negativeRole);
                    }
                }
            }
        }

        private bool TryGetRule(ulong messageID, SocketReaction reaction, out ReactionRoleModel roleModel)
        {
            if (reaction.Emote is Emote emote)
            {
                ReactionRoleModel idResult = null;
                idResult = _models.Find(r => (r.MessageID == messageID) && (r.EmojiID == emote.Id));
                roleModel = idResult;
                return idResult != null;
            }
            else
            {
                ReactionRoleModel ucResult = null;
                ucResult = _models.Find(r => (r.MessageID == messageID) && (r.EmojiUnicode == (reaction.Emote as Emoji).ToString()));
                roleModel = ucResult;
                return ucResult != null;
            }
        }
    }
}
