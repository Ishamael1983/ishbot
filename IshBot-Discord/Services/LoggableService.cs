﻿using Discord;
using System;
using System.Threading.Tasks;

namespace IshBot_Discord.Services
{
    public abstract class LoggableService : Service
    {
        public event Func<LogMessage, Task> Log;
        protected async Task ThrowLog(string text, LogSeverity severity)
        {
            string name = GetType().Name;
            LogMessage lm = new LogMessage(severity, name, text);
            if (Log != null)
                await Log.Invoke(lm);
        }
    }
}
