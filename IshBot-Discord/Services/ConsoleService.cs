﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public static class ConsoleService
    {
        public static void WriteMessage(ConsoleMessage consoleMessage)
        {
            int count = consoleMessage.Items.Count;
            for (int i = 0; i < count; i++)
            {
                Console.ForegroundColor = consoleMessage.Items[i].Colour;
                Console.Write(consoleMessage.Items[i].Text);
            }
            Console.ResetColor();
            Console.WriteLine("");
        }
    }
}
