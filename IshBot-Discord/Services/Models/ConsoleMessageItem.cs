﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord
{
    public class ConsoleMessageItem
    {
        public string Text { get; }
        public ConsoleColor Colour { get; }

        public ConsoleMessageItem(string text, ConsoleColor colour = ConsoleColor.White)
        {
            Text = text;
            Colour = colour;
        }

    }
}
