﻿using IshBot_Discord.Minesweeper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class MinesweeperWrapper
    {
        private readonly MinesweeperGenerator mg = new MinesweeperGenerator();
        private readonly string spoilerTag = "||";
        private readonly string blankTile = "\u2b1c";
        private readonly string numberSuffix = "\u20e3";
        private readonly string bombTile = ":bomb:";

        public MinesweeperWrapper() { }

        public string Help()
        {
            return "`!minesweeper [size]` where `size` is `1`, `2` or `3`.\nIf the size value is left empty, defaults to `1`.\n" +
                "Three sizes are available:\n```\n" +
                " Size | Width | Height | Bomb Count\n" +
                "   1  |   8   |    8   |     15\n" +
                "   2  |  11   |   11   |     30\n" +
                "   3  |  14   |   14   |     45\n" +
                "```";
        }

        public string Generate(int size)
        {
            int side, bombCount;
            switch (size)
            {
                default:
                case 1:
                    {
                        side = 8;
                        bombCount = 15;
                        break;
                    }
                case 2:
                    {
                        side = 11;
                        bombCount = 30;
                        break;
                    }
                case 3:
                    {
                        side = 14;
                        bombCount = 45;
                        break;
                    }
            }
            return Generate(side, side, bombCount);
        }
        public string Generate(int width = -1, int height = -1, int bombCount = -1)
        {
            width = width == -1 ? (int)mg.Width : width;
            height = height == -1 ? (int)mg.Height : height;
            bombCount = bombCount == -1 ? (int)mg.BombCount : bombCount;
            string generated = mg.Generate((uint)width, (uint)height, (uint)bombCount);
            StringBuilder sb = new StringBuilder();
            char[] chararray = generated.ToCharArray();
            for (int i = 0; i < chararray.Length; i++)
            {
                if ((i > 0) && (i % width == 0))
                    sb.Append("\n");
                if (char.IsDigit(chararray[i]))
                {
                    sb.Append(ConvertTile(Convert.ToInt32(chararray[i])));
                }
            }
            return sb.ToString();
        }

        private string ConvertTile(int value)
        {
            int valueCorrected = value - 48;
            string converted = valueCorrected == 0 ? blankTile : valueCorrected == 9 ? bombTile : valueCorrected.ToString() + numberSuffix;
            return spoilerTag + converted + spoilerTag;
        }
    }
}
