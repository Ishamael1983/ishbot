﻿namespace IshBot_Discord
{
    public class ReactionRoleModel
    {
        public string RoleName { get; set; } = "";
        public ulong ServerID { get; set; } = 0;
        public ulong MessageID { get; set; } = 0;
        public ulong EmojiID { get; set; } = 0;
        public string EmojiUnicode { get; set; } = "";
        public ulong RoleID { get; set; } = 0;
        public ulong NegativeRoleID { get; set; } = 0;
        public bool NoticeRemovals { get; set; } = false;
        public bool InvertBehaviour { get; set; } = false;
        public bool NotifyHelpRequired { get; set; } = false;
        public ulong HelpChanID { get; set; } = 0;
        public ulong HelpRoleID { get; set; } = 0;
    }
}
