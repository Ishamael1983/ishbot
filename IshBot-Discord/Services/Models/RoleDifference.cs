﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord
{
    public class RoleDifference
    {
        private readonly List<ulong> _roles;
        private readonly ActionType _type;

        public List<ulong> Roles
        {
            get { return _roles; }
        }
        public ActionType Type
        {
            get { return _type; }
        }

        public RoleDifference(List<ulong> roles, ActionType type)
        {
            _roles = roles;
            _type = type;
        }

        public enum ActionType
        {
            Removed,
            Added
        }
    }
}
