﻿using System;
using System.Collections.Generic;

namespace IshBot_Discord
{
    public class MinesweeperCooldown
    {
        public int CooldownSmall { get; set; } = 90;
        public int CooldownMedium { get; set; } = 360;
        public int CooldownLarge { get; set; } = 720;
        private readonly Dictionary<int, Dictionary<ulong, long>> CooldownLog = new Dictionary<int, Dictionary<ulong, long>>();

        public MinesweeperCooldown()
        {
            for (int i = 1; i < 5; i++)
                CooldownLog.Add(i, new Dictionary<ulong, long>());
        }

        /// <summary>
        /// Returns true if a puzzle can be sent and also initiates a new cooldown. Supply 4 for size if including a custom cooldown.
        /// </summary>
        /// <param name="size"></param>
        /// <param name="channelID"></param>
        /// <param name="customCooldown"></param>
        /// <returns></returns>
        public bool Query(int size, ulong channelID, int customCooldown = -1)
        {
            long currentTime = DateTimeOffset.Now.ToUnixTimeSeconds();
            if (CooldownLog[size].ContainsKey(channelID))
            {
                int newCooldown = (size == 4) && (customCooldown > -1) ? customCooldown : GetCooldown(size);
                long expiry = CooldownLog[size][channelID] + newCooldown;
                if (currentTime < expiry)
                    return false;
                else
                    CooldownLog[size][channelID] = currentTime;
            }
            else
                CooldownLog[size].Add(channelID, currentTime);
            return true;
        }

        public string Help()
        {
            string result = "As many people can play the same puzzles, a cooldown system is in place to prevent spam/annoyance.\n```\n" +
                "Size | Cooldown (secs)\n" +
                $"  1  |  {CooldownSmall}\n" +
                $"  2  |  {CooldownMedium}\n" +
                $"  3  |  {CooldownLarge}\n```";
            return result;
        }

        private int GetCooldown(int size)
        {
            switch (size)
            {
                case 1:
                    return CooldownSmall;
                case 2:
                    return CooldownMedium;
                case 3:
                    return CooldownLarge;
                default:
                    return -1;
            }
        }
    }
}
