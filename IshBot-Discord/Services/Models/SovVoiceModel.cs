﻿namespace IshBot_Discord
{
    public class SovVoiceModel
    {
        public ulong GuildID { get; set; } = 0;
        public ulong TextChanID { get; set; } = 0;
        public ulong RoleID { get; set; } = 0;
        public bool IsValid => (GuildID > 0) && (TextChanID > 0);
    }
}
