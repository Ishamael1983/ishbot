﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord
{
    public class MinesweeperSettings
    {
        public static int MinWidth => 5;
        public static int MaxWidth => 18;
        public int MinHeight => MinWidth;
        public int MaxHeight => Math.Min(MaxWidth, 28 - Width);
        public int Width { get; }
        public int Height { get; }
        public int BombCount { get; }
        public int Cooldown { get; }

        public MinesweeperSettings(int width, Random random)
        {
            int widthSanitised = width < MinWidth ? MinWidth : width > MaxWidth ? MaxWidth : width;
            Width = widthSanitised;
            Height = random.Next(MinHeight, MaxHeight);
            if (random.NextDouble() * 1000 > 999.9)
                BombCount = 1;
            else
                BombCount = (5 * Width * Height) / 20;
            Cooldown = (Width * Width * Height * Height) / 50;
        }
    }
}
