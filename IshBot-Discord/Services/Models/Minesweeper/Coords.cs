﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord.Minesweeper
{
    internal class Coords
    {
        internal int X { get; }
        internal int Y { get; }

        internal Coords(int x, int y)
        {
            X = x;
            Y = y;
        }

        internal Coords Add(int x, int y)
        {
            int newX = X + x;
            int newY = Y + y;
            return new Coords(newX, newY);
        }
    }
}
