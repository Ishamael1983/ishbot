﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord.Minesweeper
{
    public class MinesweeperGenerator
    {
        #region Fields and Properties
        private readonly Random random;
        private uint _width = 10;
        private uint _height = 8;
        private uint _bombCount = 10;

        public uint Width
        {
            get { return _width; }
            set
            {
                if ((value >= MinWidth) && (value < MaxWidth))
                    _width = value;
            }
        }
        public uint MinWidth => 5;
        public uint MaxWidth => 30;
        public uint Height
        {
            get { return _height; }
            set
            {
                if ((value >= MinHeight) && (value <= MaxHeight))
                    _height = value;
            }
        }
        public uint MinHeight => 5;
        public uint MaxHeight => 20;
        public uint BombCount
        {
            get { return _bombCount; }
            set
            {
                if ((value >= MinBombCount) && (value < (_width * _height)))
                    _bombCount = value;
            }
        }
        public uint MinBombCount => 1;
        public uint MaxBombCount => _width * _height;
        #endregion

        public MinesweeperGenerator()
        {
            Random r = new Random();
            random = new Random(r.Next(100000, 999999));
        }

        /// <summary>
        /// Generates a new Minesweeper map based on the default values.
        /// Result format: 9 = bomb, 0 = clear, 1-8 = danger level
        /// </summary>
        /// <returns></returns>
        public string Generate()
        {
            return Generate(_width, _height, _bombCount);
        }
        /// <summary>
        /// Generates a new Minesweeper map based on the supplied values.
        /// Result format: 9 = bomb, 0 = clear, 1-8 = danger level
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="bombCount"></param>
        /// <returns></returns>
        public string Generate(uint width, uint height, uint bombCount)
        {
            // Sanitise inputs.
            width = width < MinWidth ? MinWidth : width > MaxWidth ? MaxWidth : width;
            height = height < MinHeight ? MinHeight : height > MaxHeight ? MaxHeight : height;
            BombCount = bombCount < MinBombCount ? MinBombCount : bombCount > MaxBombCount ? MaxBombCount : bombCount;

            // Do the actual generation.
            List<uint> map = GetEmptyMap(width, height);
            PlaceBombs(ref map, bombCount);
            PlaceNumbers(ref map, width);
            
            // Convert to text block.
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < map.Count; i++)
                sb.Append(map[i].ToString());
            return sb.ToString();
        }

        private List<uint> GetEmptyMap(uint width, uint height)
        {
            List<uint> result = new List<uint>((int)(width * height));
            for (int i = 0; i < (width * height); i++)
                result.Add(0);
            return result;
        }

        private void PlaceBombs(ref List<uint> map, uint bombCount)
        {
            for (int i = 0; i < bombCount; i++)
            {
                int b = random.Next(0, map.Count);
                if (map[b] == 9)
                {
                    i--;
                    continue;
                }
                map[b] = 9;
            }
        }

        private void PlaceNumbers(ref List<uint> map, uint width)
        {
            for (int i = 0; i < map.Count; i++)
            {
                if (map[i] == 9)
                    continue;
                uint foundBombCount = GetSurroundingBombs(map, i, width);
                map[i] = foundBombCount;
            }
        }

        private uint GetSurroundingBombs(List<uint> map, int index, uint mapWidth)
        {
            uint uindex = (uint)index;
            int mapHeight = map.Count / (int)mapWidth;
            CoordsTranslator ct = new CoordsTranslator(mapWidth);
            Coords coords = ct.Translate(uindex);
            uint foundBombCount = 0;

            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    if ((x == 0) && (y == 0))
                        continue;
                    Coords newCoords = coords.Add(x, y);
                    if ((newCoords.X < 0) || (newCoords.X >= mapWidth) || (newCoords.Y < 0) || (newCoords.Y >= mapHeight))
                        continue;
                    int newIndex = ct.Translate(newCoords);
                    if ((newIndex >= map.Count) || (newIndex < 0))
                        continue;
                    if (map[newIndex] == 9)
                        foundBombCount++;
                }
            }

            return foundBombCount;
        }
    }
}
