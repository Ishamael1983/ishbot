﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord.Minesweeper
{
    internal class CoordsTranslator
    {
        internal int Width = 0;

        /// <summary>
        /// Width must be greater than zero!
        /// </summary>
        /// <param name="width"></param>
        internal CoordsTranslator(uint width)
        {
            Width = (int)width;
        }
        internal Coords Translate(uint index)
        {
            int x = (int)index % Width;
            int y = (int)index / Width;
            return new Coords(x, y);
        }
        internal int Translate(Coords coords)
        {
            return coords.Y * Width + coords.X;
        }
    }
}
