﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IshBot_Discord
{
    public class ConsoleMessage
    {
        public List<ConsoleMessageItem> Items { get; }

        public ConsoleMessage()
        {
            Items = new List<ConsoleMessageItem>();
        }

        public void AddItem(string text, ConsoleColor colour = ConsoleColor.White)
        {
            ConsoleMessageItem newItem = new ConsoleMessageItem(text, colour);
            Items.Add(newItem);
        }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Items.Count; i++)
            {
                s.Append(Items[i].Text);
            }
            return s.ToString();
        }
    }
}
