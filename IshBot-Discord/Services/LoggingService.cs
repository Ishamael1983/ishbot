﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using IshBot_Discord.Extensions;
using IshBot_Discord.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class LoggingService : Service
    {
        public event Func<LogMessage, Task> Log;
        private async Task ThrowLog(LogMessage msg)
        {
            if (Log != null)
                await Log.Invoke(msg);
        }

        private readonly IServiceProvider _provider;

        private string _logDirectory { get; }
        private string _logFile => Path.Combine(_logDirectory, $"{DateTime.UtcNow.ToString("yyyy-MM-dd")}.txt");
        public LogSeverity LogSeverity { get; set; } = LogSeverity.Info;

        public LoggingService(DiscordSocketClient discord, CommandService command, IServiceProvider provider)
        {
            _provider = provider;
            _logDirectory = Path.Combine(AppContext.BaseDirectory, "logs");

            discord.Log += OnLogAsync;
            command.Log += OnLogAsync;
        }

        private async Task OnLogAsync(LogMessage msg)
        {
            if (msg.Severity <= LogSeverity)
            {
                await ThrowLog(msg);
                if (!Directory.Exists(_logDirectory))     // Create the log directory if it doesn't exist
                    Directory.CreateDirectory(_logDirectory);
                if (!File.Exists(_logFile))               // Create today's log file if it doesn't exist
                    File.Create(_logFile).Dispose();

                string logTime = DateTimeOffset.Now.ToString();
                string logText = $"{logTime} [{msg.Severity}] {msg.Source}: {msg.Exception?.ToString() ?? msg.Message}";
                File.AppendAllText(_logFile, logText + "\n");     // Write the log text to a file

                ConsoleMessage cm = new ConsoleMessage()
                .WithItem($"{logTime} [")
                .WithItem(msg.Severity.ToString(), SeverityColour(msg.Severity))
                .WithItem($"] {msg.Source}: {msg.Exception?.ToString() ?? msg.Message}");
                ConsoleService.WriteMessage(cm);
            }
            await Task.CompletedTask;
        }

        internal void Register(IServiceCollection collection)
        {
            foreach (ServiceDescriptor s in collection)
            {
                if (s.ServiceType.BaseType == typeof(LoggableService))
                {
                    LoggableService ls = (LoggableService)_provider.GetRequiredService(s.ServiceType);
                    ls.Log += OnLogAsync;
                }                    
            }
        }

        public ConsoleColor SeverityColour(LogSeverity logSeverity)
        {
            switch (logSeverity)
            {
                case LogSeverity.Critical:
                    return ConsoleColor.Red;
                case LogSeverity.Error:
                    return ConsoleColor.DarkRed;
                case LogSeverity.Warning:
                    return ConsoleColor.Yellow;
                case LogSeverity.Verbose:
                    return ConsoleColor.DarkGray;
                case LogSeverity.Debug:
                    return ConsoleColor.DarkMagenta;
                case LogSeverity.Info:
                default:
                    return ConsoleColor.White;
            }
        }
    }
}
