﻿using Discord;
using Discord.WebSocket;
using IshBot_Discord.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class SovVoiceTagService : LoggableService
    {
        private readonly DiscordSocketClient _discord;
        private readonly Dictionary<ulong, SovVoiceModel> voiceModels = new Dictionary<ulong, SovVoiceModel>();
        private long voiceChannelScanLastRun = 0;

        public SovVoiceTagService(DiscordSocketClient discord)
        {
            _discord = discord;
            _discord.Ready += Discord_Ready;
            _discord.UserVoiceStateUpdated += Discord_UserVoiceStateUpdated;
            AddVoiceModels();
        }

        private void AddVoiceModels()
        {
            //              voice chan id
            AddVoiceModel(98664135935406080, 491610109068312586, 491617136364486658, 491988209174183944); // SOV main voice channel

            AddVoiceModel(510921862943997952, 510921864089174046, 510931807009505306); // BF Voice 1
            AddVoiceModel(510921862943997952, 510931722062397471, 510931836898246658); // BF Voice 2

            AddVoiceModel(379806642666995723, 379806643111854083, 514637355345248266, 514637215721324544); // LeePHX

            AddVoiceModel(499974030703460352, 517187523357835265, 519199670648438794); // DJTruthsayer

            AddVoiceModel(530425534773985280, 530425540750999553, 530445939245318165); // Project Perryman

            AddVoiceModel(418516310192816129, 418516548437803029, 532963126950232065); // Agent Ink - General I
            AddVoiceModel(418516310192816129, 421464044529254420, 532963157912584202); // Agent Ink - General II
            AddVoiceModel(418516310192816129, 418525309239951364, 670824411653210133); // Agent Ink - Staff

            AddVoiceModel(542374353241767938, 542377779879215105, 542377802989699105); // Project Dele

            AddVoiceModel(290659745445183489, 296766871879942144, 549625305732612096); // FC 1
            AddVoiceModel(290659745445183489, 327665616406183936, 549625331099762699); // FC 2

            AddVoiceModel(567355990702555156, 567355990702555160, 567365329291051009); // SpaceDay General

            AddVoiceModel(442055219811975168, 680488047418015907, 680488592577134725);
        }
        private void AddVoiceModel(ulong guildID, ulong voiceChanID, ulong textChanID, ulong roleID = 0)
        {
            SovVoiceModel svm = new SovVoiceModel
            {
                GuildID = guildID,
                TextChanID = textChanID,
                RoleID = roleID
            };
            voiceModels.Add(voiceChanID, svm);
        }

        private async Task Discord_Ready()
        {
            await UserScan();
        }

        private async Task Discord_UserVoiceStateUpdated(SocketUser arg1, SocketVoiceState arg2, SocketVoiceState arg3)
        {
            await ThrowLog($"UserVoiceStateUpdated : Event received for {arg1.Username}@{arg1.Discriminator}", LogSeverity.Debug);
            if (arg2.VoiceChannel != null)
            {
                bool match = false;
                if (arg3.VoiceChannel != null)
                    if (arg2.VoiceChannel.Id == arg3.VoiceChannel.Id)
                        match = true;
                if (!match)
                {
                    await ThrowLog($"UserVoiceStateUpdated : Arg2.VoiceChannel:{arg2.VoiceChannel.Id}", LogSeverity.Debug);
                    if (voiceModels.ContainsKey(arg2.VoiceChannel.Id))
                    {
                        string channelName;
                        try { channelName = " " + _discord.GetGuild(voiceModels[arg2.VoiceChannel.Id].GuildID).GetVoiceChannel(arg2.VoiceChannel.Id).Name; }
                        catch { channelName = ""; }

                        await ThrowLog("UserVoiceStateUpdated : VoiceModels contains Arg2.VoiceChannel.Id", LogSeverity.Debug);
                        await ThrowLog($"User {arg1.Username}#{arg1.Discriminator} left{channelName}.", LogSeverity.Info);
                        if (voiceModels[arg2.VoiceChannel.Id].RoleID > 0)
                        {
                            IRole newrole = _discord.GetGuild(voiceModels[arg2.VoiceChannel.Id].GuildID).GetRole(voiceModels[arg2.VoiceChannel.Id].RoleID);
                            await _discord.GetGuild(voiceModels[arg2.VoiceChannel.Id].GuildID).GetUser(arg1.Id).RemoveRoleAsync(newrole);
                        }
                        await _discord.GetGuild(voiceModels[arg2.VoiceChannel.Id].GuildID).GetTextChannel(voiceModels[arg2.VoiceChannel.Id].TextChanID).SendMessageAsync($"{arg1.Username} left.");
                    }
                }
            }

            if (arg3.VoiceChannel != null)
            {
                bool match = false;
                if (arg2.VoiceChannel != null)
                    if (arg2.VoiceChannel.Id == arg3.VoiceChannel.Id)
                        match = true;
                if (!match)
                {
                    await ThrowLog($"UserVoiceStateUpdated : Arg3.VoiceChannel:{arg3.VoiceChannel.Id}", LogSeverity.Debug);
                    if (voiceModels.ContainsKey(arg3.VoiceChannel.Id))
                    {
                        string channelName;
                        try { channelName = " " + _discord.GetGuild(voiceModels[arg3.VoiceChannel.Id].GuildID).GetVoiceChannel(arg3.VoiceChannel.Id).Name; }
                        catch { channelName = ""; }

                        await ThrowLog("UserVoiceStateUpdated : VoiceModels contains Arg3.VoiceChannel.Id", LogSeverity.Debug);
                        await ThrowLog($"User {arg1.Username}#{arg1.Discriminator} joined{channelName}.", LogSeverity.Info);
                        if (voiceModels[arg3.VoiceChannel.Id].RoleID > 0)
                        {
                            IRole newrole = _discord.GetGuild(voiceModels[arg3.VoiceChannel.Id].GuildID).GetRole(voiceModels[arg3.VoiceChannel.Id].RoleID);
                            await _discord.GetGuild(voiceModels[arg3.VoiceChannel.Id].GuildID).GetUser(arg1.Id).AddRoleAsync(newrole);
                        }
                        await _discord.GetGuild(voiceModels[arg3.VoiceChannel.Id].GuildID).GetTextChannel(voiceModels[arg3.VoiceChannel.Id].TextChanID).SendMessageAsync($"<@{arg1.Id}> joined!");
                    }
                }
            }
        }

        public async Task<string> UserScan()
        {
            long now = DateTimeOffset.Now.ToUnixTimeSeconds();
            if (now < (voiceChannelScanLastRun + 10))
            {
                long diff = (voiceChannelScanLastRun + 10) - now;
                string secs = diff == 1 ? "second" : "seconds";
                return $"Error: Too fast. Please wait {diff} {secs} and try again.";
            }
            else
                voiceChannelScanLastRun = now;
            await ThrowLog("[User Scan] Started.", LogSeverity.Verbose);
            Dictionary<ulong, IReadOnlyCollection<SocketGuildUser>> guildUsers = new Dictionary<ulong, IReadOnlyCollection<SocketGuildUser>>();
            List<ulong> guildIDs = new List<ulong>();
            StringBuilder sbResult = new StringBuilder();
            foreach (KeyValuePair<ulong, SovVoiceModel> item in voiceModels)
                if ((!guildIDs.Contains(item.Value.GuildID)) && (item.Value.RoleID > 0))
                    guildIDs.Add(item.Value.GuildID);
            if (guildIDs.Count > 0)
            {
                foreach (ulong guildID in guildIDs)
                {
                    var users = _discord.GetGuild(guildID).Users;
                    var eUsers = users.GetEnumerator();
                    int modified = 0;
                    List<ulong> joinedUsers = new List<ulong>();
                    List<string> partedUsers = new List<string>();
                    for (int i = 0; i < users.Count; i++)
                    {
                        eUsers.MoveNext();
                        ulong voiceChanId = 0;
                        voiceChanId = eUsers.Current.VoiceChannel != null ? eUsers.Current.VoiceChannel.Id : 0;
                        if (voiceChanId > 0)
                            if (voiceModels.ContainsKey(voiceChanId))
                            {
                                await ThrowLog($"[User Scan] User {eUsers.Current.Username}#{eUsers.Current.Discriminator} IS in the voice channel.", LogSeverity.Debug);
                                if (voiceModels[voiceChanId].RoleID > 0)
                                {
                                    var roles = eUsers.Current.Roles;
                                    var eRoles = roles.GetEnumerator();
                                    bool hasRole = false;
                                    for (int j = 0; j < roles.Count; j++)
                                    {
                                        eRoles.MoveNext();
                                        if (eRoles.Current.Id == voiceModels[voiceChanId].RoleID)
                                        {
                                            await ThrowLog($"[User Scan] User {eUsers.Current.Username}#{eUsers.Current.Discriminator} HAS the VoiceText role.", LogSeverity.Debug);
                                            hasRole = true;
                                            break;
                                        }
                                    }
                                    if (!hasRole)
                                    {
                                        await ThrowLog($"[UserScan] Adding VoiceText role to user {eUsers.Current.Username}#{eUsers.Current.Discriminator}.", LogSeverity.Verbose);
                                        joinedUsers.Add(eUsers.Current.Id);
                                        modified++;
                                        IRole newrole = _discord.GetGuild(guildID).GetRole(voiceModels[voiceChanId].RoleID);
                                        await eUsers.Current.AddRoleAsync(newrole);
                                    }
                                }
                            }
                            else
                            {
                                await ThrowLog($"[User Scan] User {eUsers.Current.Username}#{eUsers.Current.Discriminator} is NOT in the voice channel.", LogSeverity.Debug);
                                if (voiceModels[voiceChanId].RoleID > 0)
                                {
                                    var roles = eUsers.Current.Roles;
                                    var eRoles = roles.GetEnumerator();
                                    int _modified = modified;
                                    for (int j = 0; j < roles.Count; j++)
                                    {
                                        eRoles.MoveNext();
                                        if (eRoles.Current.Id == voiceModels[voiceChanId].RoleID)
                                        {
                                            await ThrowLog($"[User Scan] Removing VoiceText role from {eUsers.Current.Username}#{eUsers.Current.Discriminator}.", LogSeverity.Verbose);
                                            partedUsers.Add(eUsers.Current.Username);
                                            modified++;
                                            IRole newrole = _discord.GetGuild(guildID).GetRole(voiceModels[voiceChanId].RoleID);
                                            await eUsers.Current.RemoveRoleAsync(newrole);
                                        }
                                    }
                                    if (modified == _modified)
                                        await ThrowLog($"[User Scan] User {eUsers.Current.Username}#{eUsers.Current.Discriminator} does NOT have the VoiceText role.", LogSeverity.Debug);
                                }
                            }
                    }
                    await ThrowLog($"[User Scan] Completed. {modified} users (un)tagged.", LogSeverity.Info);
                    sbResult.Append($"User scan complete. {modified} users (un)tagged. ");
                    if (joinedUsers.Count > 0)
                    {
                        string hashave = joinedUsers.Count == 1 ? "has" : "have";
                        sbResult.Append($"{CompileTagList(joinedUsers)} {hashave} joined. ");
                    }
                    if (partedUsers.Count > 0)
                    {
                        string hashave = partedUsers.Count == 1 ? "has" : "have";
                        sbResult.Append($"{CompileTagList(partedUsers)} {hashave} left.");
                    }
                }
            }
            else
            {
                await ThrowLog("[User Scan] Failed. Unable to get guild users.", LogSeverity.Error);
                sbResult.Append("User scan failed. Unable to get guild users.");
            }
            return sbResult.ToString();
        }

        private string CompileTagList<T>(List<T> arg)
        {
            bool mention = true;
            StringBuilder nameListText = new StringBuilder();
            if (arg.Count > 0)
            {
                if (arg.GetType() == typeof(List<string>))
                    mention = false;
                for (int i = 0; i < arg.Count; i++)
                {
                    if (i > 0)
                    {
                        if (i == arg.Count - 1)
                            nameListText.Append(" and ");
                        else
                            nameListText.Append(", ");
                    }
                    if (mention)
                        nameListText.Append($"<@{arg[i]}>");
                    else
                        nameListText.Append($"{arg[i]}");
                }
            }
            return nameListText.ToString();
        }
    }
}
