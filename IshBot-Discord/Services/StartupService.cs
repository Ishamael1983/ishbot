﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using IshBot_Discord.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace IshBot_Discord
{
    public class StartupService : LoggableService
    {
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly IConfigurationRoot _config;
        private readonly IServiceProvider _serviceProvider;

        public StartupService(DiscordSocketClient discord, CommandService commands, IConfigurationRoot config, IServiceProvider serviceProvider)
        {
            _discord = discord;
            _commands = commands;
            _config = config;
            _serviceProvider = serviceProvider;
            //_discord.Ready += Discord_Ready;
            _discord.Disconnected += Discord_Disconnected;
        }

        private async Task Discord_Disconnected(Exception arg)
        {
            await ThrowLog($"Discord disconnected. Message: {arg.InnerException.Message}", LogSeverity.Warning);
        }

        private async Task Discord_Ready()
        {
            await _discord.SetGameAsync("Booting up...");
        }

        public async Task StartAsync()
        {
            string discordToken = _config["tokens:discord"];
            if (string.IsNullOrWhiteSpace(discordToken))
                throw new Exception("Discord token not found. Check config file.");

            await _discord.LoginAsync(TokenType.Bot, discordToken);
            await _discord.StartAsync();
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _serviceProvider);
        }
    }
}
